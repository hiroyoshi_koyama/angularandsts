package net.guides.springboot2.springboot2jpacrudexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StsSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(StsSampleApplication.class, args);
	}

}
